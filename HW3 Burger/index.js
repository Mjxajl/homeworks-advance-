`use strict`

function Hamburger(size, stuffing) {
    try {
        if (!size || !stuffing) {
            throw new HamburgerException("Size and Stuffing expected")
        }
        if (size != Hamburger.SIZE_SMALL && size != Hamburger.SIZE_LARGE) {
            throw new  HamburgerException("select correct size:Hamburger.SIZE_SMALL or Hamburger.SIZE_LARGE ")
        }
        if (stuffing != Hamburger.STUFFING_CHEESE && stuffing != Hamburger.STUFFING_SALAD && stuffing != Hamburger.STUFFING_POTATO) {
            throw new  HamburgerException("select correct stuffing: Hamburger.STUFFING_CHEESE or Hamburger.STUFFING_SALAD or Hamburger.STUFFING_POTATO ")
        }
        this.stuffing = stuffing;
        this.size = size;
    }
    catch (err) {
        console.log(err.name)
        console.log(err.message)
    }
}
Hamburger.SIZE_SMALL = {
    price: 50
    , calories: 20
    , name: "small"
}
Hamburger.SIZE_LARGE = {
    price: 100
    , calories: 40
    , name: "large"
}
Hamburger.STUFFING_CHEESE = {
    price: 10
    , calories: 20
    , name: "cheese"
}
Hamburger.STUFFING_SALAD = {
    price: 20
    , calories: 5
    , name: "salad"
}
Hamburger.STUFFING_POTATO = {
    price: 15
    , calories: 10
    , name: "potato"
}
Hamburger.TOPPING_MAYO = {
    price: 20
    , calories: 5
    , name: "mayo"
}
Hamburger.TOPPING_SPICE = {
    price: 15
    , calories: 0
    , name: "spice"
}
Hamburger.prototype.getTest = function () {
    return this.size;
}
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!topping) {
            throw new HamburgerException("Topping expected");
        }
        if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
            throw new HamburgerException("select correct TOPPING: Hamburger.TOPPING_MAYO or Hamburger.TOPPING_SPICE")
        }
        if (Array.isArray(this.toppings) && this.toppings.indexOf(topping) != -1) {
            throw new HamburgerException("duplicate topping");
        }
        this.toppings ? this.toppings.push(topping) : (this.toppings = [], this.toppings.push(topping));
    }
    catch (err) {
        console.log(err.name)
        console.log(err.message)
    }
}
Hamburger.prototype.calculatePrice = function (prop) {
    if(!prop) {prop = "price"};
    let result = 0;
    for (let key in this) {
        if (this[key][prop]) {
            result += this[key][prop]
        }
    }
    if (this.toppings) {
        for (let i = 0; i < this.toppings.length; i++) {
            result += this.toppings[i][prop];
        }
    }
    return result;
}
Hamburger.prototype.calculateCalories = function () {
    return this.calculatePrice("calories")
};
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
            throw new HamburgerException("select correct TOPPING: Hamburger.TOPPING_MAYO or Hamburger.TOPPING_SPICE")
        }
        if (this.toppings.indexOf(topping) == -1) {
            throw new HamburgerException("This burger doesn't contains this toping")
        }
        this.toppings.splice(this.toppings.indexOf(topping), 1);
        this.toppings.length === 0 ? delete this.toppings : null;
    }
    catch (err) {
        console.log(err.name)
        console.log(err.message)
    }
}
Hamburger.prototype.getSize = function () {
    return this.size.name
}
Hamburger.prototype.getToppings = function () {
    let toppings = "";
    for (let i = 0; i < this.toppings.length; i++) {
        toppings += toppings ? `, ${this.toppings[i].name}` : `${this.toppings[i].name}`;
    }
    return toppings
}
function HamburgerException (message) {
    this.name = "User Error";
    this.message = message;
}
function navListClick() {
    change = true;
    return function () {
        var elem = document.getElementById('clickMe');
        var elemInvicible = document.getElementById('invisible');
        if (window.innerWidth < 980) {
            elem.innerHTML = change ? `<i class="fas fa-times">` : `<i class="fas fa-bars"></i>`;
            elemInvicible.classList.toggle("visible");
            change = !change;
        }
    }
}
let click1 = navListClick();
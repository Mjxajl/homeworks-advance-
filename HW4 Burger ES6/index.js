`use strict`

class Hamburger {
    constructor(size,stuffing){
        try {
            if (!size || !stuffing) {
                throw new HamburgerException("Size and Stuffing expected")
            }
            if (JSON.stringify(size) != JSON.stringify(Hamburger.SIZE_SMALL) && JSON.stringify(size) != JSON.stringify(Hamburger.SIZE_LARGE)) {
                throw new  HamburgerException("select correct size:Hamburger.SIZE_SMALL or Hamburger.SIZE_LARGE ")
            }
            if (JSON.stringify(stuffing) != JSON.stringify(Hamburger.STUFFING_CHEESE) && JSON.stringify(stuffing) != JSON.stringify(Hamburger.STUFFING_SALAD) && JSON.stringify(stuffing) != JSON.stringify(Hamburger.STUFFING_POTATO)) {
                throw new  HamburgerException("select correct stuffing: Hamburger.STUFFING_CHEESE or Hamburger.STUFFING_SALAD or Hamburger.STUFFING_POTATO ")
            }
            this.stuffing = stuffing;
            this.size = size;
        }
        catch (err) {
            console.log(err.name)
            console.log(err.message)
        }
    }
 static get SIZE_SMALL() {return{
        price: 50
        , calories: 20
        , name: "small"}
    }
    static get SIZE_LARGE()  {return{
        price: 100
        , calories: 40
        , name: "large"}
    }
    static  get STUFFING_CHEESE (){return {
        price: 10
        , calories: 20
        , name: "cheese"}
    }
    static  get STUFFING_CHEESE() {return{
        price: 20
        , calories: 5
        , name: "salad"}
    }
    static   get STUFFING_POTATO() {return{
        price: 15
        , calories: 10
        , name: "potato"}
    }
    static get TOPPING_MAYO (){return{
        price: 20
        , calories: 5
        , name: "mayo"}
    }
    static get TOPPING_SPICE () {return{
        price: 15
        , calories: 0
        , name: "spice"
    }}
addTopping (topping) {
            try {
                if (!topping) {
                    throw new HamburgerException("Topping expected");
                }
                if (JSON.stringify(topping) != JSON.stringify(Hamburger.TOPPING_MAYO) && JSON.stringify(topping) != JSON.stringify(Hamburger.TOPPING_SPICE)) {
                    throw new HamburgerException("select correct TOPPING: Hamburger.TOPPING_MAYO or Hamburger.TOPPING_SPICE")
                }
                if (Array.isArray(this.toppings) && this.toppings.filter(a => (JSON.stringify(topping)==JSON.stringify(a)))[0]){
                    throw new HamburgerException("duplicate topping");
                }
                this.toppings ? this.toppings.push(topping) : (this.toppings = [], this.toppings.push(topping));
            }
            catch (err) {
                console.log(err.name)
                console.log(err.message)
            }            
}
get getToppings () {
        if (this.toppings){
        let toppings = "";
        for (let i = 0; i < this.toppings.length; i++) {
            toppings += toppings ? `, ${this.toppings[i].name}` : `${this.toppings[i].name}`;
        }
        return toppings}
        else return "there is no topings in this burger"
    }
removeTopping (topping) {
            try {
                if (JSON.stringify(topping) != JSON.stringify(Hamburger.TOPPING_MAYO) && JSON.stringify(topping) != JSON.stringify(Hamburger.TOPPING_SPICE)) {
                    throw new HamburgerException("select correct TOPPING: Hamburger.TOPPING_MAYO or Hamburger.TOPPING_SPICE")
                }
                if (this.toppings.map(a=>JSON.stringify(a),0).indexOf(JSON.stringify(topping)) == -1) {
                    throw new HamburgerException("This burger doesn't contains this toping")
                }
                this.toppings.splice(this.toppings.map(a=>JSON.stringify(a),0).indexOf(JSON.stringify(topping)), 1);
                this.toppings.length === 0 ? delete this.toppings : null;
            }
                catch (err) {
        console.log(err.name)
        console.log(err.message)
    }    
        }
    get getSize () {
        return this.size.name;
    }


get getPrice () {
        let result = 0;
        for (let key in this) {
            if (this[key].price) {
                result += this[key].price
            }
        }
        if (this.toppings) {
            for (let i = 0; i < this.toppings.length; i++) {
                result += this.toppings[i].price;
            }
        }
        return result;
    }
get getCalories ()     { 
       let result = 0;
for (let key in this) {
    if (this[key].calories) {
        result += this[key].calories
    }
}
if (this.toppings) {
    for (let i = 0; i < this.toppings.length; i++) {
        result += this.toppings[i].calories;
    }
}
return result;
}
}
    function HamburgerException (message) {
    this.name = "User Error";
    this.message = message;
}




